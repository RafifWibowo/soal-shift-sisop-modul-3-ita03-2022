# soal-shift-sisop-modul-3-ITA03-2022

Laporan pengerjaan soal shift modul 1 Praktikum Sistem Operasi 2022 Kelompok ITA03

## Anggota Kelompok:

1. Salsabila Briliana Ananda Sofi - 5027201003
2. Rafif Naufaldi Wibowo - 5027201010
3. Fairuz Azka Maulana - 5027201017

## Soal 1

### Study case soal 1

---

Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

### Problem

---

- Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.
- Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.
- Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.
- Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)
- Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

Catatan:

- Boleh menggunakan execv(), tapi bukan mkdir, atau system
- Jika ada pembenaran soal, akan di-update di catatan
- Kedua file .zip berada di folder modul
- Nama user di passwordnya adalah nama salah satu anggota kelompok, baik yang mengerjakan soal tersebut atau tidak. Misalkan satu kelompok memiliki anggota namanya Yudhistira, Werkudara, dan Janaka. Yang mengerjakan adalah Janaka. Nama passwordnya bisa mihinomenestyudhistira, mihinomenestwerkudara, atau mihinomenestjanaka
- Menggunakan apa yang sudah dipelajari di Modul 3 dan, kalau perlu, di Modul 1 dan 2

### Solution

---

[Source Code](./soal1/)

### 1.a

Melakukan unzip pada file quote.zip dan music.zip denngan menggunakan thread pada fungsi dari unzip untuk mengekstrak file dari quote.zip dan music.zip. Setelah melakukan eksraksi zip kemudian thread akan dilakukan pemanggilan pada fungsi main dan melakukan join untuk kedua fungsi.

```c
char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* base64_decode(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(cipher) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';
    return plain;
}

void unzip_file(char name_file[1000]) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        char *argv[] = {"unzip","-q", name_file, NULL};
        execv("/bin/unzip", argv);
        exit(EXIT_SUCCESS);

    } else {
        wait(NULL);
    }
}

while(i < 2) {
		err1 = pthread_create(&(tid_unzip[i]),NULL,&thread_unzip,NULL);
		if(err1!=0) {
			printf("\n can't create thread : [%s]",strerror(err1));
		} else {
			printf("\n create thread success\n");
		}

		i++;
	}

    pthread_join(tid_unzip[0],NULL);
	pthread_join(tid_unzip[1],NULL);

```

### 1.b

Untuk melakukan decode maka dibutuhkan fungsi dari base64_decode untuk menerjemahkan file ke karakter ASCII. Yang kemudian base_decode tersebut akan dilakukan fungsi pemanggilan untuk menjalankan proses pengisian pada file music.txt dan quote.txt. Sebelum itu file music.txt dan quote.txt harus dibuat terlebih dahulu yang kemudian akan dimasukan hasil dari decode file.

```c
char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* base64_decode(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(cipher) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';
    return plain;
}
```

Kemudian untuk mengisi file quote.txt akan digunakan fungsi tid_file. Kemudian thread tid_file akan dipanggil pada fungsi main untuk melakukan join.

```c
} else if(pthread_equal(id, tid_file[1])) {
        create_file("quote.txt");
        FILE* ptr, *fp;
        char str[50];
        char word[100];
        char files[100][100];
        strcpy(files[0], "q1.txt");
        strcpy(files[1], "q2.txt");
        strcpy(files[2], "q3.txt");
        strcpy(files[3], "q4.txt");
        strcpy(files[4], "q5.txt");
        strcpy(files[5], "q6.txt");
        strcpy(files[6], "q7.txt");
        strcpy(files[7], "q8.txt");
        strcpy(files[8], "q9.txt");

        for(int i = 0; i<9; ++i) {
            FILE* ptr, *fp;
            char str[50];
            ptr = fopen(files[i], "r");
            fp = fopen("quote.txt", "a");

            if (NULL == ptr) {
                    printf("file can't be opened \n");
                }

                while (fgets(str, 100, ptr) != NULL) {
                    fprintf(fp, "%s\n", base64_decode(str));
                    fclose(fp);
                }

                fclose(ptr);
            }

while(j < 2) {
		err2 = pthread_create(&(tid_file[j]),NULL,&thread_create_file,NULL);
		if(err2!=0) {
			printf("\n can't create thread : [%s]",strerror(err2));

		} else {
			printf("\n create thread success\n");
		}

		j++;
	}


    pthread_join(tid_file[0],NULL);
	pthread_join(tid_file[1],NULL);

```

### 1.c

Untuk memindahkan fille-file tersebut dibutuhkan sebuah direktori yang dinamai hasil pada fungsi utama yang kemudian akan memindahkan file tersebut dengan menambahkan fungsi tid_file pada file music.txt dan quote.txt

```c
if(child_id == 0) {
        char *argv_hasil[] = {"mkdir", "-p", "hasil", NULL};
        execv("/bin/mkdir", argv_hasil);
    } else {
        wait(NULL);
    }
fclose(ptr);
        }


    if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_music[] = {"mv", "music.txt", "hasil/music.txt", NULL};
        execv("/bin/mv", argv_music);

    } else {
        wait(NULL);
    }
}
else if(pthread_equal(id, tid_file[1]))
	{
		fclose(ptr);
      }

  if(child_id < 0) exit(EXIT_FAILURE);

  if(child_id == 0) {
      char *argv_quote[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
      execv("/bin/mv", argv_quote);

  } else {
      wait(NULL);
  }
```

### 1.d

Untuk melakukan set password pada folder hasil maka fungsi main ditambahkan fungsi strcpy(zip_password kemudian menambahkan fungsi pid_t pada fungsi child.

```c
char zip_password[1000];
    strcpy(zip_password, "minihomenestsarah");
    strcat(zip_password, getlogin());

    pid_t child_id_1;
    child_id_1 = fork();

    if(child_id_1 < 0) exit(EXIT_FAILURE);

    if(child_id_1 == 0) {
        char *argv_password_zip[] = {"zip", "-qrP", zip_password, "hasil.zip", "hasil", NULL};
        execv("/bin/zip", argv_password_zip);
    } else {
        wait(NULL);
    }

```

### 1.e

Pada pembuatan 2 thread baru menggunakan tid_unzip_file untuk file yang hasil.zip dan no.txt dan akan membuatkan no. nya kemudian pada fungsi main akan dibuar preprocess untuk melakukan set passwordnya.

```c
pthread_t tid_unzip_no_file[2];
void * unzip_no_file(void *arg) {
    pthread_t id=pthread_self();
    pid_t child_id;
    child_id = fork();

    if(pthread_equal(id,tid_unzip_no_file[0])) {
        char zip_password[1000];
        strcpy(zip_password, "minihomenestsarah");
        strcat(zip_password, getlogin());
        if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            char *argv_unzip[] = {"unzip", "-qo", "-P", zip_password, "hasil.zip", NULL};
            execv("/bin/unzip", argv_unzip);
        } else {
            wait(NULL);
        }
    } else if(pthread_equal(id,tid_unzip_no_file[1])) {
        child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            create_file("no.txt");
            FILE* fp = fopen("no.txt", "a");
            fprintf(fp, "No");
            fclose(fp);
        }
    }
}
```

```c
if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_zip[] = {"zip", "-qrP", zip_password, "hasil.zip", "./hasil", "./no.txt", NULL};
        execv("/bin/zip", argv_zip);
    }
```

# soal-shift-sisop-modul-3-ITA03-2022

Laporan pengerjaan soal shift modul 1 Praktikum Sistem Operasi 2022 Kelompok ITA03

## Anggota Kelompok:

---

1. Salsabila Briliana Ananda Sofi - 5027201003
2. Rafif Naufaldi Wibowo - 5027201010
3. Fairuz Azka Maulana - 5027201017

## Soal 2

---

### Problem

---

Bluemary adalah seorang Top Global 1 di salah satu platform online judge. Suatu hari Ia ingin membuat online judge nya sendiri, namun dikarenakan Ia sibuk untuk mempertahankan top global nya, maka Ia meminta kamu untuk membantunya dalam membuat online judge sederhana. Online judge sederhana akan dibuat dengan sistem client-server dengan beberapa kriteria sebagai berikut:

- Pada saat client terhubung ke server, terdapat dua pilihan pertama yaitu register dan login. Jika memilih register, client akan diminta input id dan passwordnya untuk dikirimkan ke server. Data input akan disimpan ke file **users.txt** dengan format **username:password**. Jika client memilih login, server juga akan meminta client untuk input id dan passwordnya lalu server akan mencari data di **users.txt** yang sesuai dengan input client. Jika data yang sesuai ditemukan, maka client dapat login dan dapat menggunakan command-command yang ada pada sistem. Jika tidak maka server akan menolak login client. Username dan password memiliki kriteria sebagai berikut:

  - Username unique (tidak boleh ada user yang memiliki username yang sama)
  - Password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil
  - Format **users.txt**:
    > username:password <br>
    > username2:pass2

- Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama **problems.tsv** yang terdiri dari **judul problem dan author problem (berupa username dari author), yang dipisah dengan \t**. File otomatis dibuat saat server dijalankan.

- **Client yang telah login**, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input yaitu:

  - Judul problem (unique, tidak boleh ada yang sama dengan problem lain)
  - Path file description.txt pada client (file ini berisi deskripsi atau penjelasan problem)
  - Path file input.txt pada client (file ini berguna sebagai input testcase untuk menyelesaikan problem)
  - Path file output.txt pada client (file ini berguna untuk melakukan pengecekan pada submission client terhadap problem)
  - client-side:
    > add
  - server-side:
    > Judul problem: <br>
    > Filepath description.txt: <br>
    > Filepath input.txt: <br>
    > Filepath output.txt: <br>
  - client-side:

    > judul-problem1 <br>
    > Client/description.txt-1 <br>
    > Client/input.txt-1 <br>
    > Client/output.txt-1

  - Seluruh file akan disimpan oleh server ke dalam folder dengan nama <judul-problem> yang di dalamnya terdapat file description.txt, input.txt dan output.txt. Penambahan problem oleh client juga akan mempengaruhi file **problems.tsv**.

- **Client yang telah login**, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut:

  > judul-problem-1 by author1 <br>
  > judul-problem-2 by author2

- **Client yang telah login**, dapat memasukkan command ‘download judul-problem’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu **judul-problem**. Kedua file tersebut akan disimpan ke folder dengan nama **judul-problem** di client.

- **Client yang telah login**, dapat memasukkan command ‘submit judul-problem path-file-output.txt’. Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

- Server dapat menangani multiple-connection. Dimana jika terdapat 2 atau lebih client yang terhubung ke server, maka harus menunggu sampai client pertama keluar untuk bisa melakukan login dan mengakses aplikasinya.

### Code

---

### client.c

```c
#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 8080

int main(int argc, char const *argv[])
{
    struct sockaddr_in address;
    int sock = 0;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }
    memset(&serv_addr, '0', sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    while (1)
    {
        char input[50];
        memset(buffer, 0, 1024);
        read(sock, buffer, 1024);
        printf("%s", buffer);
        memset(buffer, 0, 1024);
        scanf("%[^\n]%*c", input);
        send(sock, input, strlen(input), 0);
        memset(buffer, 0, 1024);
    }
    return 0;
}
```

### server.c

```c
#include <stdio.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#define PORT 8080

int checkUsr(char user[100])
{
    char db[100] = "user.txt";
    int flag = 1;
    FILE *file = fopen(db, "r");

    if (!file)
    {
        flag = 2;
    }

    char line[1000];

    while (fgets(line, sizeof(line), file))
    {
        const char sep[2] = ":";
        char *token;

        token = strtok(line, sep);
        if (strcmp(token, user) == 0)
        {
            flag = -1;
        }
    }

    fclose(file);

    return flag;
}

int checkPw(char pass[100])
{
    int flag = 1;
    int low = 0;
    int up = 0;
    int num = 0;
    int len = strlen(pass);
    for (int i = 0; i < len; i++)
    {
        if (isupper(pass[i]))
        {
            up++;
        }
        if (islower(pass[i]))
        {
            low++;
        }
        if (isdigit(pass[i]))
        {
            num++;
        }
    }
    if (up == 0 | low == 0 | num == 0 | len < 6)
    {
        flag = -1;
    }

    return flag;
}

void reg(char user[100], char pass[100])
{
    char db[100] = "user.txt";
    FILE *file = fopen(db, "a");

    char content[100];
    strcpy(content, user);
    strcat(content, ":");
    strcat(content, pass);
    strcat(content, "\n");

    fprintf(file, content);
    fclose(file);
}

int login(char user[100], char pass[100])
{
    int find = -1;
    char search[100];
    strcpy(search, user);
    strcat(search, ":");
    strcat(search, pass);
    strcat(search, "\n");
    char db[100] = "user.txt";
    FILE *file = fopen(db, "r");

    char line[1000];

    while (fgets(line, sizeof(line), file))
    {
        if (strcmp(search, line) == 0)
        {
            find = 1;
            break;
        }
    }

    fclose(file);

    return find;
}

void addTitle(char title[100], char user[100])
{
    char db[100] = "problem.tsv";
    FILE *file = fopen(db, "a");

    char content[100];
    strcpy(content, title);
    strcat(content, "\t");
    strcat(content, user);
    strcat(content, "\n");

    fprintf(file, content);
    fclose(file);
}

void makeDir(char path[100])
{
    mkdir(path, S_IRWXU);
}

void makeFile(char path[100], char dest[100])
{
    char ch;

    FILE *source, *target;

    source = fopen(path, "r");

    if (source == NULL)
    {
        printf("1");
        exit(EXIT_FAILURE);
    }

    target = fopen(dest, "w");

    if (target == NULL)
    {
        fclose(source);
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
}

void download(char title[100])
{
    FILE *fp;
    char desc_path[100] = "/home/kali/modul3/soal2/Client";
    strcat(desc_path, "/");
    strcat(desc_path, title);
    makeDir(desc_path);
    strcat(desc_path, "/");
    strcat(desc_path, "description.txt");

    char input_path[100] = "/home/kali/modul3/soal2/Client";
    strcat(input_path, "/");
    strcat(input_path, title);
    makeDir(input_path);
    strcat(input_path, "/");
    strcat(input_path, "input.txt");

    char server_desc[100] = "/home/kali/modul3/soal2/Server";
    strcat(server_desc, "/");
    strcat(server_desc, title);
    strcat(server_desc, "/");
    strcat(server_desc, "description.txt");
    // printf(desc_path);
    makeFile(server_desc, desc_path);

    char server_input[100] = "/home/kali/modul3/soal2/Server";
    // strcpy(server_path, title);
    // strcat(server_path, "/");
    // strcat(server_path, "input.txt");
    strcat(server_desc, "/");
    strcat(server_desc, title);
    strcat(server_desc, "/");
    strcat(server_desc, "input.txt");
    // printf(input_path);
    makeFile(server_input, input_path);
}

int main(int argc, char const *argv[])
{
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    // message for client
    // starting point
    char *start = "register/login? ";
    char *mintaUser = "\nusername: ";
    char *mintaPw = "password: ";

    // error exist
    char *usrExist = "username exist";
    char *passInvalid = "password invalid";
    char *errLogin = "invalid username/password";

    // command
    char *cmd = "add/see/download/submit: ";

    // add
    char *mintaTitle = "Judul problem: ";
    char *mintaDesc = "Filepath description.txt: ";
    char *mintaInput = "Filepath input.txt: ";
    char *mintaOutput = "Filepath output.txt: ";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    FILE *usr, *prob;
    usr = fopen("user.txt", "a");
    fclose(usr);
    prob = fopen("problem.tsv", "a");
    fclose(prob);
    while (1)
    {
        send(new_socket, start, strlen(start), 0);
        memset(buffer, 0, 1024);
        read(new_socket, buffer, 1024);

        if (strcmp(buffer, "register") == 0)
        {
            char usr[100], pw[100];
            memset(buffer, 0, 1024);
            send(new_socket, mintaUser, strlen(mintaUser), 0);
            read(new_socket, buffer, 1024);
            strcpy(usr, buffer);
            int statusUsr;
            statusUsr = checkUsr(usr);

            if (statusUsr == -1)
            {
                send(new_socket, usrExist, strlen(usrExist), 0);
                exit(EXIT_FAILURE);
            }

            memset(buffer, 0, 1024);
            send(new_socket, mintaPw, strlen(mintaPw), 0);
            read(new_socket, buffer, 1024);
            strcpy(pw, buffer);
            int statusPw;
            statusPw = checkPw(pw);

            if (statusPw == -1)
            {
                send(new_socket, passInvalid, strlen(passInvalid), 0);
                exit(EXIT_FAILURE);
            }

            reg(usr, pw);
        }
        if (strcmp(buffer, "login") == 0)
        {
            while (1)
            {
                char user[100], pass[100];
                memset(buffer, 0, 1024);
                send(new_socket, mintaUser, strlen(mintaUser), 0);
                read(new_socket, buffer, 1024);
                strcpy(user, buffer);

                memset(buffer, 0, 1024);
                send(new_socket, mintaPw, strlen(mintaPw), 0);
                read(new_socket, buffer, 1024);
                strcpy(pass, buffer);

                int status = login(user, pass);

                if (status == -1)
                {
                    memset(buffer, 0, 1024);
                    send(new_socket, errLogin, strlen(errLogin), 0);
                }
                else if (status == 1)
                {
                    while (1)
                    {
                        char command[10];
                        memset(buffer, 0, 1024);
                        send(new_socket, cmd, strlen(cmd), 0);
                        read(new_socket, buffer, 1024);
                        strcpy(command, buffer);

                        char *cmd = strtok(command, " ");

                        if (strcmp(cmd, "add") == 0)
                        {
                            char title[100], desc[100], input[100], output[100];

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaTitle, strlen(mintaTitle), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(title, buffer);

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaDesc, strlen(mintaDesc), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(desc, buffer);

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaInput, strlen(mintaInput), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(input, buffer);

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaOutput, strlen(mintaOutput), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(output, buffer);

                            makeDir(title);

                            char descPath[100], inputPath[100], outputPath[100];

                            strcpy(descPath, title);
                            strcat(descPath, "/description.txt");
                            makeFile(desc, descPath);

                            strcpy(inputPath, title);
                            strcat(inputPath, "/input.txt");
                            makeFile(input, inputPath);

                            strcpy(outputPath, title);
                            strcat(outputPath, "/output.txt");
                            makeFile(output, outputPath);

                            addTitle(title, user);
                        }
                        else if (strcmp(cmd, "see") == 0)
                        {
                            char prob[100] = "problem.tsv";
                            FILE *file = fopen(prob, "r");

                            char line[1000];

                            while (fgets(line, sizeof(line), file))
                            {
                                const char sep[10] = "\t";
                                char *token;
                                char title[100], author[100], content[100];

                                token = strtok(line, sep);
                                strcpy(title, token);

                                token = strtok(NULL, sep);
                                strcpy(author, token);

                                strcpy(content, title);
                                strcat(content, " by ");
                                strcat(content, author);

                                send(new_socket, content, strlen(content), 0);
                            }
                            fclose(file);
                        }
                        else if (strcmp(cmd, "download") == 0)
                        {
                            cmd = strtok(NULL, " ");
                            char title[100];
                            strcpy(title, cmd);
                            download(title);
                        }
                    }
                }
            }
        }
    }
    return 0;
}
```

### Explanation

---

### client .c

Bagian utamanya ada pada line berikut.

```c
while (1)
{
  char input[50];
  memset(buffer, 0, 1024);
  read(sock, buffer, 1024);
  printf("%s", buffer);
  memset(buffer, 0, 1024);
  scanf("%[^\n]%*c", input);
  send(sock, input, strlen(input), 0);
  memset(buffer, 0, 1024);
}
```

Setiap akan menerima input, buffer akan di reset agar isi dari buffer menjadi kosong. Kemudian, client akan membaca message dari server dan menampilkannya di terminal. Setelah itu, buffer akan di reset untuk menyimpan inputan yang akan dimasukkan. Input yang didapat akan dikirim kembali ke server.

### server.c

---

Berikut ada beberapa fungsi yang digunakan untuk menjalankan program **server.c**.

- checkUsr
  <br>Fungsi ini digunakan untuk mengecek apakah user yang didaftarkan sudah ada atau belum. Fungsi ini akan mengecek dengan membaca tiap line yang ada pada database **user.txt**. Bila ada yang sama akan me-return status = -1. bila tidak akan me-return status = 1.

  ```c
  int checkUsr(char user[100])
  {
      char db[100] = "user.txt";
      int flag = 1;
      FILE *file = fopen(db, "r");

      if (!file)
      {
          flag = 2;
      }

      char line[1000];

      while (fgets(line, sizeof(line), file))
      {
          const char sep[2] = ":";
          char *token;

          token = strtok(line, sep);
          if (strcmp(token, user) == 0)
          {
              flag = -1;
          }
      }

      fclose(file);

      return flag;
  }
  ```

- checkPw
  <br> Fungsi ini digunakan untuk mengecek apakah password yang akan digunakan sudah memenuhi kriteria atau belum. Bila memenuhi kriteria akan me-return status = 1. Sebaliknya, jika tidak memenuhi akan me-return status = -1.

  ```c
  int checkPw(char pass[100])
  {
      int flag = 1;
      int low = 0;
      int up = 0;
      int num = 0;
      int len = strlen(pass);
      for (int i = 0; i < len; i++)
      {
          if (isupper(pass[i]))
          {
              up++;
          }
          if (islower(pass[i]))
          {
              low++;
          }
          if (isdigit(pass[i]))
          {
              num++;
          }
      }
      if (up == 0 | low == 0 | num == 0 | len < 6)
      {
          flag = -1;
      }

      return flag;
  }
  ```

- reg
  <br>
  Fungsi ini digunakan untuk menjalankan tugas registrasi. Fungsi akan mengakses file **user.txt**. Kemudian akan disusun konten agar sesuai dengan soal, yaitu **useername:pass**. Konten yang sudah disusun akan dimasukkan ke dalam file **user.txt**.

  ```c
  void reg(char user[100], char pass[100])
  {
    char db[100] = "user.txt";
    FILE \*file = fopen(db, "a");

    char content[100];
    strcpy(content, user);
    strcat(content, ":");
    strcat(content, pass);
    strcat(content, "\n");

    fprintf(file, content);
    fclose(file);
  }
  ```

- login
  <br>
  Fungsi ini digunakan untuk menjalankan tugas login. Fungsi akan mengakses file **user.txt**. Sebelum diakses, akan disusun konten yang sesuai pada isi **user.txt** agar memudahkan pencarian, yaitu **useername:pass**. Kemudian akan dilakukan pencarian pada file. Jika sudah ditemukan, akan mereturn status = 1. Sebaliknya, jika tidak ditemukan akan mereturn status = -1.

  ```c
  int login(char user[100], char pass[100])
  {
    int find = -1;
    char search[100];
    strcpy(search, user);
    strcat(search, ":");
    strcat(search, pass);
    strcat(search, "\n");
    char db[100] = "user.txt";
    FILE *file = fopen(db, "r");

    char line[1000];

    while (fgets(line, sizeof(line), file))
    {
        if (strcmp(search, line) == 0)
        {
            find = 1;
            break;
        }
    }

    fclose(file);

    return find;
  }
  ```

- addTitle
  <br>
  Fungsi ini digunakan untuk menambahkan **problem** serta **author** yang membuat program tersebut ke dalam **problem.tsv**.

  ```c
  void addTitle(char title[100], char user[100])
  {
    char db[100] = "problem.tsv";
    FILE *file = fopen(db, "a");

    char content[100];
    strcpy(content, title);
    strcat(content, "\t");
    strcat(content, user);
    strcat(content, "\n");

    fprintf(file, content);
    fclose(file);
  }
  ```

- makeDir
  <br>
  Fungsi ini digunakan untuk membuat **directory** baru berdasarkan **path** yang di passing.

  ```c
  void makeDir(char path[100])
  {
    mkdir(path, S_IRWXU);
  }
  ```

- makeFile
  <br>
  Fungsi ini digunakan untuk membuat **file** baru berdasarkan **path** yang di passing. Isi dari **file** tersebut merupakan hasil copy dari isi file **source** yang di passing.

  ```c
  void makeFile(char path[100], char dest[100])
  {
    char ch;

    FILE *source, *target;

    source = fopen(path, "r");

    if (source == NULL)
    {
        printf("1");
        exit(EXIT_FAILURE);
    }

    target = fopen(dest, "w");

    if (target == NULL)
    {
        fclose(source);
        exit(EXIT_FAILURE);
    }

    while ((ch = fgetc(source)) != EOF)
        fputc(ch, target);

    fclose(source);
    fclose(target);
  }
  ```

- download
  <br>
  Fungsi ini digunakan untuk menjalankan command download. Fungsi akan membuat **directory** pada directory **client** berdasarkan judul problem yang diinputkan. Isi dari dari directory adalah file description dan input yang berasal dari directory **server**.

  ```c
  void download(char title[100])
  {
    FILE *fp;
    char desc_path[100] = "/home/kali/modul3/soal2/Client";
    strcat(desc_path, "/");
    strcat(desc_path, title);
    makeDir(desc_path);
    strcat(desc_path, "/");
    strcat(desc_path, "description.txt");

    char input_path[100] = "/home/kali/modul3/soal2/Client";
    strcat(input_path, "/");
    strcat(input_path, title);
    makeDir(input_path);
    strcat(input_path, "/");
    strcat(input_path, "input.txt");

    char server_desc[100] = "/home/kali/modul3/soal2/Server";
    strcat(server_desc, "/");
    strcat(server_desc, title);
    strcat(server_desc, "/");
    strcat(server_desc, "description.txt");
    // printf(desc_path);
    makeFile(server_desc, desc_path);

    char server_input[100] = "/home/kali/modul3/soal2/Server";
    // strcpy(server_path, title);
    // strcat(server_path, "/");
    // strcat(server_path, "input.txt");
    strcat(server_desc, "/");
    strcat(server_desc, title);
    strcat(server_desc, "/");
    strcat(server_desc, "input.txt");
    // printf(input_path);
    makeFile(server_input, input_path);
  }
  ```

- main

```c
int main(int argc, char const *argv[])
{
    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};

    // message for client
    // starting point
    char *start = "register/login? ";
    char *mintaUser = "\nusername: ";
    char *mintaPw = "password: ";

    // error exist
    char *usrExist = "username exist";
    char *passInvalid = "password invalid";
    char *errLogin = "invalid username/password";

    // command
    char *cmd = "add/see/download/submit: ";

    // add
    char *mintaTitle = "Judul problem: ";
    char *mintaDesc = "Filepath description.txt: ";
    char *mintaInput = "Filepath input.txt: ";
    char *mintaOutput = "Filepath output.txt: ";

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)))
    {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(PORT);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0)
    {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0)
    {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    FILE *usr, *prob;
    usr = fopen("user.txt", "a");
    fclose(usr);
    prob = fopen("problem.tsv", "a");
    fclose(prob);
    while (1)
    {
        send(new_socket, start, strlen(start), 0);
        memset(buffer, 0, 1024);
        read(new_socket, buffer, 1024);

        if (strcmp(buffer, "register") == 0)
        {
            char usr[100], pw[100];
            memset(buffer, 0, 1024);
            send(new_socket, mintaUser, strlen(mintaUser), 0);
            read(new_socket, buffer, 1024);
            strcpy(usr, buffer);
            int statusUsr;
            statusUsr = checkUsr(usr);

            if (statusUsr == -1)
            {
                send(new_socket, usrExist, strlen(usrExist), 0);
                exit(EXIT_FAILURE);
            }

            memset(buffer, 0, 1024);
            send(new_socket, mintaPw, strlen(mintaPw), 0);
            read(new_socket, buffer, 1024);
            strcpy(pw, buffer);
            int statusPw;
            statusPw = checkPw(pw);

            if (statusPw == -1)
            {
                send(new_socket, passInvalid, strlen(passInvalid), 0);
                exit(EXIT_FAILURE);
            }

            reg(usr, pw);
        }
        if (strcmp(buffer, "login") == 0)
        {
            while (1)
            {
                char user[100], pass[100];
                memset(buffer, 0, 1024);
                send(new_socket, mintaUser, strlen(mintaUser), 0);
                read(new_socket, buffer, 1024);
                strcpy(user, buffer);

                memset(buffer, 0, 1024);
                send(new_socket, mintaPw, strlen(mintaPw), 0);
                read(new_socket, buffer, 1024);
                strcpy(pass, buffer);

                int status = login(user, pass);

                if (status == -1)
                {
                    memset(buffer, 0, 1024);
                    send(new_socket, errLogin, strlen(errLogin), 0);
                }
                else if (status == 1)
                {
                    while (1)
                    {
                        char command[10];
                        memset(buffer, 0, 1024);
                        send(new_socket, cmd, strlen(cmd), 0);
                        read(new_socket, buffer, 1024);
                        strcpy(command, buffer);

                        char *cmd = strtok(command, " ");

                        if (strcmp(cmd, "add") == 0)
                        {
                            char title[100], desc[100], input[100], output[100];

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaTitle, strlen(mintaTitle), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(title, buffer);

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaDesc, strlen(mintaDesc), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(desc, buffer);

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaInput, strlen(mintaInput), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(input, buffer);

                            memset(buffer, 0, 1024);
                            send(new_socket, mintaOutput, strlen(mintaOutput), 0);
                            read(new_socket, buffer, 1024);
                            strcpy(output, buffer);

                            makeDir(title);

                            char descPath[100], inputPath[100], outputPath[100];

                            strcpy(descPath, title);
                            strcat(descPath, "/description.txt");
                            makeFile(desc, descPath);

                            strcpy(inputPath, title);
                            strcat(inputPath, "/input.txt");
                            makeFile(input, inputPath);

                            strcpy(outputPath, title);
                            strcat(outputPath, "/output.txt");
                            makeFile(output, outputPath);

                            addTitle(title, user);
                        }
                        else if (strcmp(cmd, "see") == 0)
                        {
                            char prob[100] = "problem.tsv";
                            FILE *file = fopen(prob, "r");

                            char line[1000];

                            while (fgets(line, sizeof(line), file))
                            {
                                const char sep[10] = "\t";
                                char *token;
                                char title[100], author[100], content[100];

                                token = strtok(line, sep);
                                strcpy(title, token);

                                token = strtok(NULL, sep);
                                strcpy(author, token);

                                strcpy(content, title);
                                strcat(content, " by ");
                                strcat(content, author);

                                send(new_socket, content, strlen(content), 0);
                            }
                            fclose(file);
                        }
                        else if (strcmp(cmd, "download") == 0)
                        {
                            cmd = strtok(NULL, " ");
                            char title[100];
                            strcpy(title, cmd);
                            download(title);
                        }
                    }
                }
            }
        }
    }
    return 0;
}
```

### Kendala

---

Praktikan hanya dapat membuat program yang menjalankan tugas regsiter, login dan command see.

### Revisi

---

Revisi yang dilakukan adalah dengan menambahkan line code sehingga program dapat menjalankan tugas command add dan download.

### Test Case

---

- Register
  <br><br>
  ![register](img/regist.png)
  <br>  
  Akan diinputkan ke dalam **user.txt**
  <br><br>
  ![Output Register](img/regist_output.png)

- Login dan Add
  <br><br>
  ![Login dan Add](img/loginAdd.png)
  <br>
  Hasil dari Add akan membuat directory sesuai Judul dan isinya
  <br><br>
  ![Output Add](img/addOutput.png)

- See
  <br>
  Saat see diinputkan akan menampilkan semua **problem** beserta **authornya**
  <br><br>
  ![Output See](img/seeOutput.png)

- Download
  <br>
  Saat download beserta judul problem diinputkan, akan membuat directory pada client beserta isinya.
  <br><br>
  ![Output Download](img/donwloadOutput.png)
