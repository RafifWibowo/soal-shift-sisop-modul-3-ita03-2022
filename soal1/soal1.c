#include<stdio.h>
#include<string.h>
#include<sys/wait.h>
#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>
#include<sys/types.h>

char base46_map[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                     'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                     'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* base64_decode(char* cipher) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(cipher) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; cipher[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46_map[k] != cipher[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';
    return plain;
}


void unzip_file(char name_file[1000]) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        char *argv[] = {"unzip","-q", name_file, NULL};
        execv("/bin/unzip", argv);
        exit(EXIT_SUCCESS);

    } else {
        wait(NULL);
    }
}

char files[100][10];

void create_file(char *name) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        char *argv[] = {"touch", name, NULL};
        execv("/bin/touch", argv);
        
    } else {
        wait(NULL);
    }
}


pthread_t tid_unzip[2];
void *thread_unzip(void *arg) {
    pthread_t id=pthread_self();

    if(pthread_equal(id,tid_unzip[0])) {
        unzip_file("music.zip");
    } else if(pthread_equal(id,tid_unzip[1])) {
        unzip_file("quote.zip");
    }
    return NULL;
}

pthread_t tid_file[2];
void * thread_create_file(void *arg) {
    pthread_t id=pthread_self();
    pid_t child_id;
    child_id = fork();
    if(pthread_equal(id,tid_file[0])) {
        create_file("music.txt");
        FILE* ptr, *fp;
        char word[100];
        char files[100][100];
        strcpy(files[0], "m1.txt");
        strcpy(files[1], "m2.txt");
        strcpy(files[2], "m3.txt");
        strcpy(files[3], "m4.txt");
        strcpy(files[4], "m5.txt");
        strcpy(files[5], "m6.txt");
        strcpy(files[6], "m7.txt");
        strcpy(files[7], "m8.txt");
        strcpy(files[8], "m9.txt");

        for(int i = 0; i<9; ++i) {
            FILE* ptr, *fp;
            char str[50];
            ptr = fopen(files[i], "r");
            fp = fopen("music.txt", "a");
            
            if (NULL == ptr) {
                    printf("file can't be opened \n");
            }
                
                while (fgets(str, 100, ptr) != NULL) {
                    fprintf(fp, "%s\n", base64_decode(str));
                    fclose(fp);
                }    
                fclose(ptr);    
            }
        

        if(child_id < 0) exit(EXIT_FAILURE);

        if(child_id == 0) {
            char *argv_music[] = {"mv", "music.txt", "hasil/music.txt", NULL};
            execv("/bin/mv", argv_music);

        } else {
            wait(NULL);
        }
    } else if(pthread_equal(id, tid_file[1])) {
        create_file("quote.txt");
        FILE* ptr, *fp;
        char str[50];
        char word[100];
        char files[100][100];
        strcpy(files[0], "q1.txt");
        strcpy(files[1], "q2.txt");
        strcpy(files[2], "q3.txt");
        strcpy(files[3], "q4.txt");
        strcpy(files[4], "q5.txt");
        strcpy(files[5], "q6.txt");
        strcpy(files[6], "q7.txt");
        strcpy(files[7], "q8.txt");
        strcpy(files[8], "q9.txt");

        for(int i = 0; i<9; ++i) {
            FILE* ptr, *fp;
            char str[50];
            ptr = fopen(files[i], "r");
            fp = fopen("quote.txt", "a");
            
            if (NULL == ptr) {
                    printf("file can't be opened \n");
                }
                
                while (fgets(str, 100, ptr) != NULL) {
                    fprintf(fp, "%s\n", base64_decode(str));
                    fclose(fp);
                }    

                fclose(ptr);    
            }

        if(child_id < 0) exit(EXIT_FAILURE);

        if(child_id == 0) {
            char *argv_quote[] = {"mv", "quote.txt", "hasil/quote.txt", NULL};
            execv("/bin/mv", argv_quote);

        } else {
            wait(NULL);
        }
    }
}

pthread_t tid_unzip_no_file[2];
void * unzip_no_file(void *arg) {
    pthread_t id=pthread_self();
    pid_t child_id;
    child_id = fork();

    if(pthread_equal(id,tid_unzip_no_file[0])) {
        char zip_password[1000];
        strcpy(zip_password, "minihomenestsarah");
        strcat(zip_password, getlogin());
        if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            char *argv_unzip[] = {"unzip", "-qo", "-P", zip_password, "hasil.zip", NULL};
            execv("/bin/unzip", argv_unzip);
        } else {
            wait(NULL);
        }
    } else if(pthread_equal(id,tid_unzip_no_file[1])) {
        child_id = fork();
        if(child_id < 0) exit(EXIT_FAILURE);
        if(child_id == 0) {
            create_file("no.txt");
            FILE* fp = fopen("no.txt", "a");
            fprintf(fp, "No");
            fclose(fp);
        }
    }
}

int main() {

 int err1, err2, err3;
    pid_t child_id;
    child_id = fork();
    
    if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_hasil[] = {"mkdir", "-p", "hasil", NULL};
        execv("/bin/mkdir", argv_hasil);
    } else {
        wait(NULL);
    }

    int j = 0;
    int i = 0;
    int k = 0;

    //thread
 while(i < 2) {
  err1 = pthread_create(&(tid_unzip[i]),NULL,&thread_unzip,NULL); 
  if(err1!=0) {
   printf("\n can't create thread : [%s]",strerror(err1));
  } else {
   printf("\n create thread success\n");
  }

  i++;
 }

    pthread_join(tid_unzip[0],NULL);
 pthread_join(tid_unzip[1],NULL);

    sleep(1);

 while(j < 2) {
  err2 = pthread_create(&(tid_file[j]),NULL,&thread_create_file,NULL); 
  if(err2!=0) {
   printf("\n can't create thread : [%s]",strerror(err2));
            
  } else {
   printf("\n create thread success\n");
  }

  j++;
 }


    pthread_join(tid_file[0],NULL);
 pthread_join(tid_file[1],NULL);

    sleep(1);

    char zip_password[1000];
    strcpy(zip_password, "minihomenestsarah");
    strcat(zip_password, getlogin());

    pid_t child_id_1;
    child_id_1 = fork();

    if(child_id_1 < 0) exit(EXIT_FAILURE);

    if(child_id_1 == 0) {
        char *argv_password_zip[] = {"zip", "-qrP", zip_password, "hasil.zip", "hasil", NULL};
        execv("/bin/zip", argv_password_zip);
    } else {
        wait(NULL);
    }

    sleep(1);

    while(k < 2) {
  err3 = pthread_create(&(tid_unzip_no_file[k]),NULL,&unzip_no_file,NULL); 
  if(err3!=0) {
   printf("\n can't create thread : [%s]",strerror(err3));
            
  } else {
   printf("\n create thread success\n");
  }

  k++;
 }

    pthread_join(tid_unzip_no_file[0],NULL);
 pthread_join(tid_unzip_no_file[1],NULL);

    child_id = fork();

    if(child_id < 0) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_zip[] = {"zip", "-qrP", zip_password, "hasil.zip", "./hasil", "./no.txt", NULL};
        execv("/bin/zip", argv_zip);
    }

    exit(EXIT_SUCCESS);
 return 0;
}